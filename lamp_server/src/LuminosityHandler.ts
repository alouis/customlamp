import axios from "axios";

class LuminosityHandler {
	private luminosity: number;
	private currentWorker: NodeJS.Timeout;
	constructor() {
		this.luminosity = 0;
	}

	getLuminosity() {
		return this.luminosity;
	}

	async setLuminosity(newLuminosityLevel: number) {
		this.luminosity = newLuminosityLevel;
		await this.sendToArduino();
	}

	private async sendToArduino() {
		// call API w/ param = this.luminosity
		// In the meantime for more clarity
		try {
			if (this.luminosity >= 0 && this.luminosity <= 99) {
				console.log("Luminosity level = ", this.luminosity, "%");
				axios({
					method: "get",
					url: `http://arduino/?power=${this.luminosity.toString().padStart(2, "0")}`,
				});
			} else {
				console.log("Luminosity level out of range: ", this.luminosity, "%");
			}
		} catch (error) {
			console.error("Error sending luminosity level to Arduino: ", error);
		}
	}

	private async increaseLuminosity() {
		this.luminosity += 1;
		await this.sendToArduino();
	}

	start() {
		console.log("Starting luminosity handler");
		this.currentWorker = setInterval(async () => {
			await this.increaseLuminosity();
			if (this.luminosity >= 99) {
				clearInterval(this.currentWorker);
			}
		}, 15000);
	}

	turnLightOff() {
		this.luminosity = 0;
		clearInterval(this.currentWorker);
		this.sendToArduino();
	}
}

export default LuminosityHandler;
