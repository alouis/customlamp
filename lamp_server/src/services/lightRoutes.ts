import express, { Request, Response } from "express";
import dayjs from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
dayjs.extend(localizedFormat);
import LuminosityHandler from "../LuminosityHandler";

const luminosityHandler = new LuminosityHandler();

const router = express.Router();

router.put("/", async (req: Request, res: Response) => {
	try {
		const { intensity } = req.body;
		if (intensity === undefined || intensity < 0 || intensity > 100) {
			res.status(400).json({ error: "Intensity expressed in the range 0 - 100 is required." });
			return;
		}
		if (intensity === 0) {
			luminosityHandler.turnLightOff();
		} else {
			await luminosityHandler.setLuminosity(intensity);
		}
		res.status(201).end();
	} catch (e) {
		res.status(500).json(JSON.stringify(e));
	}
});

export default router;
