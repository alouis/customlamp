import { Client } from "pg";
import conf from "../config";

const client = new Client({
	user: conf.db.user,
	host: conf.db.host,
	database: conf.db.database,
	port: conf.db.port,
	password: conf.db.password
});

export const getActiveAlarms = async () => {
	const ret = await client.query(
		// 'SELECT "alarm_id", EXTRACT(HOUR FROM "startAt") AS "hour", EXTRACT(MINUTES FROM "startAt") AS "minutes" FROM schedule WHERE active IS TRUE'
		'SELECT "id", "name", "startAt", "frequency" FROM alarms WHERE "active" IS TRUE',
	);
	return ret?.rows ? ret.rows : [];
};

export const getCurrentActiveAlarms = async (day: string, hour: number, minutes: number) => {
	const query = `SELECT * FROM alarms WHERE "active" IS true AND (frequency->'${day}')::boolean IS true AND EXTRACT (HOUR FROM "startAt")='${hour}' AND EXTRACT (MINUTES FROM "startAt")='${minutes}'`;
	console.log(query);
	const ret = await client.query(query);
	return ret?.rows ? ret.rows : [];
};

export const getAllAlarms = async () => {
	const ret = await client.query("SELECT * FROM alarms");
	return ret?.rows ? ret.rows : [];
};

export const createAlarm = async (
	name: string,
	startAt: string,
	frequency: { Sun: boolean; Mon: boolean; Tue: boolean; Wed: boolean; Thu: boolean; Fri: boolean },
) => {
	const query = `INSERT INTO alarms ("name", "startAt", "active", "frequency") VALUES ('${name}', '${startAt}', true, '${JSON.stringify(
		frequency,
	)}') RETURNING "id"`;
	const newAlarmId = await client.query(query);
	return newAlarmId.rows[0].id;
};

export const deleteAlarm = async (id: string) => {
	await client.query('DELETE FROM alarms WHERE "id"=$1', [id]);
};

export const setAlarmStatus = async (id: string, active: boolean) => {
	await client.query('UPDATE alarms SET "active"=$1 WHERE "id"=$2', [active, id]);
};

export const updateAlarmName = async (id: string, newName: string) => {
	await client.query(`UPDATE alarms SET "name"=$1 WHERE "id"=$2`, [newName, id]);
};

export const updateAlarmStartingTime = async (id: string, newStartingTime: string) => {
	await client.query(`UPDATE alarms SET "startAt"=$1 WHERE "id"=$2`, [newStartingTime, id]);
};

export const updateAlarmFrequency = async (
	id: string,
	newFrequency: {
		Sun: boolean;
		Mon: boolean;
		Tue: boolean;
		Wed: boolean;
		Thu: boolean;
		Fri: boolean;
	},
) => {
	await client.query(`UPDATE alarms SET "frequency"=$1 WHERE "id"=$2`, [newFrequency, id]);
};

export default client;
