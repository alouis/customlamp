import express, { Request, Response } from "express";
import * as db from "./db";
import minuteWorker from "../MinuteWorker";
import dayjs from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
dayjs.extend(localizedFormat);
import LuminosityHandler from "../LuminosityHandler";

const luminosityHandler = new LuminosityHandler();

const router = express.Router();

router.get("/", async (req: Request, res: Response) => {
	try {
		const ret = await db.getAllAlarms();
		const allAlarms = ret.map((alarm) => {
			const { frequency, ...others } = alarm;
			const activeDays: string[] = [];
			if (alarm.frequency) {
				Object.entries(alarm.frequency).forEach(([key, value]) => {
					if (value) activeDays.push(`${key}`);
				});
			}
			return { ...others, activeDays };
		});
		console.log(allAlarms);
		res.status(200).send(allAlarms);
	} catch (e) {
		res.status(500).json(JSON.stringify(e));
	}
});

// TEST ONLY
// router.get("/active", async (req: Request, res: Response) => {
// 	try {
// 		const ret = await db.getActiveAlarms();
// 		const allAlarms = ret.map((alarm) => {
// 			const { frequency, ...others } = alarm;
// 			const activeDays: string[] = [];
// 			if (alarm.frequency) {
// 				Object.entries(alarm.frequency).forEach(([key, value]) => {
// 					if (value) activeDays.push(`${key}`);
// 				});
// 			}
// 			return { ...others, activeDays };
// 		});
// 		res.status(200).send(allAlarms);
// 	} catch (e) {
// 		res.status(500).json(JSON.stringify(e));
// 	}
// });

// router.get("/current", async (req: Request, res: Response) => {
// 	try {
// 		const now = dayjs();
// 		const currentDay = now.format("ddd");
// 		const currentHour = now.format("H");
// 		const currentMinutes = now.format("m");
// 		const ret = await db.getCurrentActiveAlarms(
// 			currentDay,
// 			Number(currentHour),
// 			Number(currentMinutes)
// 		);
// 		console.log(ret)
// 		const allAlarms = ret.map((alarm) => {
// 			const { frequency, ...others } = alarm;
// 			const activeDays: string[] = [];
// 			if (alarm.frequency) {
// 				Object.entries(alarm.frequency).forEach(([key, value]) => {
// 					if (value) activeDays.push(`${key}`);
// 				});
// 			}
// 			return { ...others, activeDays };
// 		});
// 		res.status(200).send(allAlarms);
// 	} catch (e) {
// 		res.status(500).json(JSON.stringify(e));
// 	}
// });

router.post("/", async (req: Request, res: Response) => {
	try {
		console.log(req.body);
		const { name, startAt, activeDays } = req.body;
		const frequency = { Sun: false, Mon: false, Tue: false, Wed: false, Thu: false, Fri: false, Sat: false };
		activeDays.forEach((activeDay: string) => {
			if (activeDay in frequency) frequency[activeDay] = true;
		});
		const newAlarmId = await db.createAlarm(name, startAt, frequency);
		res.status(200).json({ id: newAlarmId });
	} catch (e) {
		console.error(e);
		res.status(500).send(JSON.stringify(e));
	}
});

router.delete("/", async (req: Request, res: Response) => {
	try {
		minuteWorker.stopAlarm();
		res.status(201).end();
	} catch (e) {
		res.status(500).json(JSON.stringify(e));
	}
});

router.delete("/:id", async (req: Request, res: Response) => {
	try {
		await db.deleteAlarm(req.params.id);
		res.status(201).end();
	} catch (e) {
		res.status(500).json(JSON.stringify(e));
	}
});

router.put("/:id", async (req: Request, res: Response) => {
	try {
		const { id } = req.params;
		if (req.body.name !== undefined) {
			if (!req.body.name || req.body.name === "") throw new Error("Alarm should have a name.");
			else await db.updateAlarmName(id, req.body.name);
		}
		if (req.body.active !== undefined) {
			await db.setAlarmStatus(id, req.body.active);
		}
		if (req.body.startAt) {
			await db.updateAlarmStartingTime(id, req.body.startAt);
		}
		if (req.body.activeDays) {
			const frequency = { Sun: false, Mon: false, Tue: false, Wed: false, Thu: false, Fri: false, Sat: false };
			req.body.activeDays.forEach((activeDay: string) => {
				if (activeDay in frequency) frequency[activeDay] = true;
			});
			await db.updateAlarmFrequency(id, frequency);
		}
		res.status(201).end();
	} catch (e) {
		res.status(500).json(JSON.stringify(e));
	}
});

router.put("/test", async (req: Request, res: Response) => {
	console.log("tttttest");
	res.status(200).end();
});

router.put("/light", async (req: Request, res: Response) => {
	try {
		console.log("hello ");
		console.log(req.body);
		const { intensity } = req.body;
		if (intensity === undefined || intensity < 0 || intensity > 100) {
			res.status(400).json({ error: "Intensity expressed in the range 0 - 100 is required." });
			return;
		}
		if (intensity === 0) {
			luminosityHandler.turnLightOff();
		} else {
			luminosityHandler.setLuminosity(intensity);
		}
		res.status(201).end();
	} catch (e) {
		res.status(500).json(JSON.stringify(e));
	}
});

export default router;
