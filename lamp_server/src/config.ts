import "dotenv/config";

const conf = {
	port: process.env.PORT || 3000,
	db: {
		host: "localhost",
		database: "lampdb",
		port: 5432,
		user: "morgan",
		password: "morganitos"
	},
};

export default conf;
