import express from "express";
import bodyParser from "body-parser";
import routes from "./services/routes";
import lightRoutes from "./services/lightRoutes";
import clientDB from "./services/db";
import minuteWorker from "./MinuteWorker";

const port = 3001;
const app = express();
app.use(bodyParser.json());
app.use("/alarms", routes);
app.use("/light", lightRoutes);
let server;

const start = async (): Promise<void> => {
	await clientDB.connect();
	minuteWorker.start();

	if (server) return server;
	await new Promise((resolve) => {
		server = app.listen(port, () => {
			console.info({ message: `Listening to port ${port}` });
			resolve(undefined);
		});
	});

	return server;
};

const stop = async (): Promise<void> => {
	try {
		await new Promise<void>((res, rej) => {
			server.close((err) => (err ? rej(err) : res()));
		});
		// nothig to do here
		// eslint-disable-next-line no-empty
	} catch {}
	console.debug("server stopped");
};

export { start, stop };

// if (require.main === module) {
start();
process.on("SIGINT", async () => {
	console.debug("Intercepting SIGINT");
	await stop();
	process.exit(0);
});
// }
