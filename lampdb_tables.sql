
CREATE TABLE public.alarms (
	id serial4 NOT NULL,
	"name" varchar NULL,
	"startAt" time NULL,
	active bool NULL,
	frequency jsonb NULL,
	CONSTRAINT alarms_id_key UNIQUE (id)
);
