import { Alarm } from "@/types/alarm";
import axios from "axios";

const api = axios.create({
	baseURL: "http://raspberrypi:3000/api/",
});

export const getAlarms = async () => {
	return api.get<Alarm[]>("/alarms");
};

export const deleteAlarm = async (alarmId: string) => {
	return api.delete<Alarm>(`/alarms/${alarmId}`);
};

export const updateAlarm = async (alarmId: string, data: Partial<Alarm>) => {
	return api.put<Alarm>(`/alarms/${alarmId}`, data);
};

export const createAlarm = async (data: Omit<Alarm, "id">) => {
	return api.post<Alarm>("/alarms", data);
};

export const stopAlarm = async () => {
	return api.delete<Alarm[]>(`/alarms`);
};
