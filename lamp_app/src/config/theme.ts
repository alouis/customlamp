import { Palette, createTheme } from "@mui/material/styles";

const baseTheme = createTheme();
const customTheme = createTheme(baseTheme, {
	transitions: { duration: { enteringScreen: 200 } },

	typography: {
		h4: {
			[baseTheme.breakpoints.down("md")]: {
				fontSize: "1.5rem",
			},
		},
		body1: {
			[baseTheme.breakpoints.down("md")]: {
				fontSize: "0.9rem",
			},
		},
	},
});

const lightTheme = createTheme(customTheme, {
	palette: {
		background: {
			default: "rgba(251, 250, 255, 1)",
			paper: "rgba(255, 255, 255, 1)",
		},
	},
});

const darkTheme = createTheme(customTheme, {
	palette: {
		mode: "dark",
		text: {
			primary: "#fff",
			secondary: "rgba(255, 255, 255, 0.7)",
			disabled: "rgba(255, 255, 255, 0.5)",
		},
		action: {
			active: "#fff",
			disabled: "rgba(255, 255, 255, 0.3)",
			hover: "rgba(255, 255, 255, 0.08)",
			disabledBackground: "rgba(255, 255, 255, 0.12)",
			selected: "rgba(255, 255, 255, 0.16)",
		},
		background: {
			default: "#121212",
			paper: "#121212",
		},
		divider: "rgba(255, 255, 255, 0.12)",
	},
});

export { lightTheme, darkTheme };
