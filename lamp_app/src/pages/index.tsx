import * as React from "react";
import { updateAlarm, getAlarms, createAlarm, deleteAlarm, stopAlarm } from "@/services/nextApi";
import {
	Box,
	Button,
	Card,
	CardActionArea,
	Container,
	Dialog,
	IconButton,
	Paper,
	Stack,
	Typography,
} from "@mui/material";
import { useEffect } from "react";
import { Alarm, weekDaysName } from "@/types/alarm";
import Switch from "@/components/Switch";
import DialogEditAlarm from "@/components/DialogEditAlarm";
import dayjs from "dayjs";
import { AddCircle } from "@mui/icons-material";

const buildDefaultAlarm = (): Alarm => ({
	id: "",
	name: "",
	active: true,
	startAt: dayjs().format("HH:mm"),
	activeDays: [],
});

const HomePage = () => {
	const [selectedAlarm, setSelectedAlarm] = React.useState<Alarm | null>(null);
	const [alarms, setAlarms] = React.useState<Alarm[]>([]);
	const loadAlarms = async () => {
		const { data } = await getAlarms();
		setAlarms(data);
	};
	useEffect(() => {
		loadAlarms();
	}, []);
	const handleUpdate = async (alarm: Alarm) => {
		await updateAlarm(alarm.id!, alarm);
		await loadAlarms();
	};
	const handleCreate = async (alarm: Alarm) => {
		await createAlarm(alarm);
		await loadAlarms();
	};
	const handleDelete = async (id: string) => {
		await deleteAlarm(id);
		await loadAlarms();
	};
	const handleStopAlarm = async () => {
		await stopAlarm();
	};
	return (
		<>
			<Container sx={{ display: "flex", flexDirection: "column", alignItems: "stretch" }}>
				<Typography variant="h2" align="center" sx={{ pt: 2 }}>
					{"L'alarme du love"}
				</Typography>
				<Box p={1} />
				<Button sx={{ alignSelf: "stretch" }} variant="outlined" onClick={handleStopAlarm}>
					Alarm Off
				</Button>
				<Box p={1} />

				{alarms
					.sort((al1, al2) => dayjs(al1.startAt, "HH:mm").diff(dayjs(al2.startAt, "HH:mm")))
					.map((alarm) => {
						const time = dayjs(alarm.startAt, "HH:mm");
						return (
							<Card key={alarm.id} sx={{ p: 1, my: 1 }}>
								<Stack flexDirection="row" alignItems="center" justifyContent="space-between">
									<CardActionArea>
										<Stack
											sx={{
												paddingLeft: 0,
												display: "flex",
												flexDirection: "column",
												alignItems: "flex-start",
											}}
											onClick={() => setSelectedAlarm(alarm)}
										>
											<Typography variant="caption" sx={{ paddingLeft: 1 }}>
												{alarm.name}
											</Typography>
											<Typography variant="h2">{time.format("HH:mm")}</Typography>
										</Stack>

										<Typography sx={{ paddingLeft: 1 }}>
											{alarm.activeDays.map((day) => weekDaysName[day]).join(", ")}
										</Typography>
									</CardActionArea>
									<Switch
										size="medium"
										checked={alarm.active}
										onChange={async (e) => {
											e.stopPropagation();
											handleUpdate({ ...alarm, active: !alarm.active });
										}}
										onClick={(e) => e.stopPropagation()}
									/>
								</Stack>
							</Card>
						);
					})}
				<Stack>
					<IconButton
						color="primary"
						size="large"
						sx={{ alignSelf: "center" }}
						onClick={() => setSelectedAlarm(buildDefaultAlarm())}
					>
						<AddCircle sx={{ fontSize: 60 }} />
					</IconButton>
				</Stack>
			</Container>
			<DialogEditAlarm
				open={!!selectedAlarm}
				onClose={() => {
					setSelectedAlarm(null);
				}}
				alarm={selectedAlarm}
				onSave={(alarm) => {
					alarm.id ? handleUpdate(alarm) : handleCreate(alarm);
				}}
				onDelete={handleDelete}
			/>
		</>
	);
};

export default HomePage;
