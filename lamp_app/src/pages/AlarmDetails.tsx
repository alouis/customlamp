import * as React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { StaticTimePicker } from "@mui/x-date-pickers/StaticTimePicker";

import { createAlarm } from "@/services/nextApi";
import { Button } from "@mui/material";
import dayjs, { Dayjs } from "../services/time";

const HomePage = () => {
	const setAlarm = async (value: Dayjs | null) => {
		await createAlarm({
			name: "alarm",
			startAt: dayjs(value).format("HH:mm"),
			active: false,
			activeDays: [],
		});
	};

	return (
		<LocalizationProvider dateAdapter={AdapterDayjs}>
			<StaticTimePicker orientation="portrait" onAccept={setAlarm} />
			<Button onClick={() => {}}>Clear</Button>
		</LocalizationProvider>
	);
};

export default HomePage;
