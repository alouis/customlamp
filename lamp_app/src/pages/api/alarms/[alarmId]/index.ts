// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { deleteAlarm, updateAlarm } from "@/services/serverApi";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	const { alarmId } = req.query;

	if (req.method === "DELETE") {
		const ret = await deleteAlarm(alarmId as string);
		return res.status(200).json(ret.data);
	} else if (req.method === "PUT") {
		const data = req.body;
		const ret = await updateAlarm(alarmId as string, data);
		return res.status(200).json(ret.data);
	}
	res.status(400).end();
}
