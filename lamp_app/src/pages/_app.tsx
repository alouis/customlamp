import MyThemeProvider from "@/context/myThemeContext";
import { CssBaseline } from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import type { AppProps } from "next/app";

export default function App({ Component, pageProps }: AppProps) {
	return (
		<LocalizationProvider dateAdapter={AdapterDayjs}>
			<MyThemeProvider>
				<CssBaseline />
				<Component {...pageProps} />
			</MyThemeProvider>
		</LocalizationProvider>
	);
}
